from os import name
from django.urls import path
from . import views

#url path
urlpatterns = [ 

    path('', views.getRoutes, name="routes"),
    path('courses/',views.getCourses , name = "courses"),
    path('classes/',views.getClasses , name = "classes"),
    path('student/',views.getStudent , name = "student"),
    path('Teacher/',views.getTeacher, name = "Teacher"),
    path('Subject/',views.getSubject, name = "Subject"),
    path('Results/',views.getResults, name = "Results"),
    path('Attendance/',views.getAttendance, name = "Attendance"),
    path('AttendanceReport/',views.getAttendanceReport, name = "AttendanceReport"),
    path('Appointment/',views.getAppointment, name = "Appointment"),
    path('Notifications/',views.getNotifications, name = "Notifications"),

  ]