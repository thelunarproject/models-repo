from django.shortcuts import render
from .models import Appointment, Attendance, AttendanceReport, Course, Classes, Notifications, Results, Student, Subject, Teacher
from .serializers import AttendanceReportSerializer, AttendanceSerializer, CourseSerializer,ClassesSerializer,StudentSerializer, SubjectSerializer, TeacherSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.

@api_view(['GET'])
def getRoutes(request):
  routes = [
    '/api/courses/'
    '/api/classes/'
    '/api/student/'
    '/api/Teacher/'
    '/api/Subject/'
    '/api/Results/'
    '/api/Attendance/'
    '/api/AttendanceReport/'
    '/api/Appointmeent/'
    '/api/Notifications/'
  ]
  return Response (routes)


@api_view(['GET'])
def getCourses(request):
    courses = Course.objects.all()
    serializer = CourseSerializer(courses, many =True)
    return Response(serializer.data)


@api_view(['GET'])
def getClasses(request):
    classes = Classes.objects.all()
    serializer = ClassesSerializer(classes, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getStudent(request):
    student = Student.objects.all()
    serializer = StudentSerializer(student, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getTeacher(request):
    student = Teacher.objects.all()
    serializer = TeacherSerializer(Teacher, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getSubject(request):
    student = Subject.objects.all()
    serializer = SubjectSerializer(Subject, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getResults(request):
    student = Results.objects.all()
    serializer = Results(Results, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getAttendance(request):
    student = Attendance.objects.all()
    serializer = AttendanceSerializer(Attendance, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getAttendanceReport(request):
    student = AttendanceReport.objects.all()
    serializer = AttendanceReportSerializer(Attendance, many =True)
    return Response(serializer.data)
  
@api_view(['GET'])
def getAppointment(request):
    student = Appointment.objects.all()
    serializer = Appointment(Attendance, many =True)
    return Response(serializer.data)

@api_view(['GET'])
def getNotifications(request):
    student = Notifications.objects.all()
    serializer = Notifications(Notifications, many =True)
    return Response(serializer.data)