from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Course)
admin.site.register(Classes)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Subject)
admin.site.register(Results)
admin.site.register(Attendance)
admin.site.register(AttendanceReport)
admin.site.register(Appointment)
admin.site.register(Notifications)